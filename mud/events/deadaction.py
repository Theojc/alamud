# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event1, Event3

class DeadAction(Event1):
    NAME = "dead-action"

    def perform(self):
        self.inform("dead-action")

class FireWithEvent(Event3):
    NAME = "fire-action"

    def perform(self):
        self.inform("fire-action")
